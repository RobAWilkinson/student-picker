var students = [
  "Alex Mason",
  "Alfred Kirakosian",
  "Ben Sam",
  "Bonnie Kim",
  "Daniel Kim",
  "Daniel Mendoza",
  "David Moupin",
  "David Petri",
  "Edwin Alegre",
  "Eyal Benmoshe",
  "Greg Rock",
  "James Kelly",
  "James Kim",
  "Jim Clark",
  "Joseph Chua",
  "Kelsey Jacobsen",
  "Maggie Wang",
  "Mariano Echegoyen",
  "Peter Krouse",
  "Rami Burpee",
  "Scott Shin",
  "Steven Chow"
]

// Returns a random integer between min (included) and max (excluded)
// Using Math.round() will give you a non-uniform distribution!
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

// gets a random student
function getRandomStudent(students) {
  if(students.length > 1){
    return students[getRandomInt(0, students.length)];
  }
  else{
    return "No more Students";
  }
}
// removes a student
function removeStudent(student, students){
  students.splice(students.indexOf(student),1);
}

function updateDomWithStudentName() {
  var student = getRandomStudent(students);
  removeStudent(student, students);
  document.getElementById("student-name").innerHTML = student;
}
// gets a pair
function getPair(students) {
  var pair = [];
  var student = getRandomStudent(students);
  removeStudent(student, students);
  pair.push(student);
  student = getRandomStudent(students);
  removeStudent(student, students);
  pair.push(student);
  return pair;
}
// Updates Dom with pair
function updateDomWithPair() {
  var pair = getPair(students);
  document.getElementById("pair1").innerHTML = pair[0];
  document.getElementById("pair2").innerHTML = pair[1];
}

window.onload = function() {
  document.getElementById("pick-new-student").onclick = function() {
    updateDomWithStudentName()
  }
  document.getElementById("pair-picker").onclick = function() {
    updateDomWithPair();
  }
}
